<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Retencion extends Model
{
    use Notifiable;

    // tabla retensiones
    protected $table = 'retenciones';

    /**
     * 
     * 
     * 
     * @var array
     */
    protected $fillable = [
        'cobro_disposicion','descripcion','concepto'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
