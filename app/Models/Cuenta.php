<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Cuenta extends Model
{
    //Modelo que almacena las cuentas contables de un usuario
    use Notifiable;

    
    protected $table = 'cuentas';

    /**
     * 
     *
     * @var array --> Contiene el nombre de las columnas de la tabla 'cuentas'
     */
    protected $fillable = [
        'num_cuenta','clabe','banco','rfc','total_credito','total_debito','retenciones_id','tipocuenta_id', 'usuario_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

   
    // Relacion a los Modelos
    // Cobro de retencion por transaccion ---> foreign key retenciones_id
    public function retencion()
    {
        return $this->belongsTo('App\Models\Retencion', 'retenciones_id');
    }
    // Tipo de cuenta Credito - Debito ----> foreign key tipocuenta_id
    public function tipo_de_cuenta()
    {
        return $this->belongsTo('App\Models\TipodeCuenta', 'tipocuenta_id');
    }
    // Usuario  ----> foreing key usuario_id
    public function usuario()
    {
        return $this->belongsTo('App\User', 'usuario_id');
    }
    
}
