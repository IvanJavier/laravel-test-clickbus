<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        $this->call(UserSeeder::class);
        $this->call(TipodeCuentaSeeder::class);
        $this->call(RetencionSeeder::class);
        $this->call(CuentaSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(RoleUserSeeder::class);
        //$this->call(Transaccion::class);
        
        
    }
}
