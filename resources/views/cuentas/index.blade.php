{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Control de Cuentas</h1>
@stop

@section('content')
    
    <div class="box box-solid box-primary">
        <h4>Bienvenido: {{ Auth::user()->nombre }} {{ Auth::user()->apellido_paterno }}  {{ Auth::user()->apellido_materno }} </h4>
    </div>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        
        <thead>
            <tr>
                <th>Numero de Cuenta</th>
                <th>Clabe</th>
                <th>Institución</th>
                <th>RFC</th>
                <th>Saldo</th>
                <th>Tipo Cuenta</th>
                <th>Operaciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($cuentas as $cuenta)
                <tr>
                <td>{{ $cuenta->num_cuenta}}</td>
                <td>{{ $cuenta->clabe}}</td>
                <td>{{ $cuenta->banco}}</td>
                <td>{{ $cuenta->rfc}}</td>
                @if ($cuenta->tipocuenta_id==1)
                <td>{{ $cuenta->total_credito}}</td> 
                <td>Credito</td> 
                @else
                <td>{{ $cuenta->total_debito}}</td> 
                <td>Debito</td>     
                @endif
                @if ($cuenta->tipocuenta_id==1)
                <td><a class="btn btn-primary" href="{{ route('cuentas.retiro_credito')}}">Retiro TDC</a></td> 
                @else
                <td><a class="btn btn-primary" href="{{ route('cuentas.retiro_debito')}}">Retiro TDD</a></td> 
                @endif
                </tr>
            @endforeach
                
        </tbody>
    </table>
    <a href="{{route('home')}}">< Regresar</a>
    {{ $cuentas->links() }}
   
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop