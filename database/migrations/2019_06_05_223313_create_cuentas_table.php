<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_cuenta');
            $table->string('clabe');
            $table->string('banco');
            $table->string('rfc');
            $table->float('total_credito')->nullable();
            $table->float('total_debito')->nullable();
            $table->unsignedInteger('retenciones_id')->nullable();
            $table->unsignedInteger('tipocuenta_id')->nullable();
            $table->unsignedInteger('usuario_id');
            $table->foreign('retenciones_id')->references('id')->on('retenciones');
            $table->foreign('tipocuenta_id')->references('id')->on('tipo_de_cuentas');
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas');
    }
}
