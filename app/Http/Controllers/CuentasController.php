<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Cuenta;
use App\Models\Retencion;
use App\Models\Transaccion;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;


class CuentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['usuario']);
        $cuentas = Cuenta::where('usuario_id', Auth::user()->id)->paginate(10);
        //dd($cuentas);
        return view('cuentas.index', compact('cuentas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cuentas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function retiroCredito(Request $request)
    {
        $request->user()->authorizeRoles(['usuario']);
        $cuentas = Cuenta::where('usuario_id', Auth::user()->id)->where('tipocuenta_id',1)->paginate(1);
        $transacciones = Transaccion::where('usuario_id', Auth::user()->id)->where('cuenta_id',1)->paginate(10);
        return view('cuentas.retiro_credito', ['cuentas' => $cuentas, 'transacciones' => $transacciones]);
        
    }

    public function retiroDebito(Request $request)
    {
        $request->user()->authorizeRoles(['usuario']);
        $cuentas = Cuenta::where('usuario_id', Auth::user()->id)->where('tipocuenta_id',2)->paginate(1);
        $transacciones = Transaccion::where('usuario_id', Auth::user()->id)->where('cuenta_id',2)->paginate(10);
        return view('cuentas.retiro_debito', ['cuentas' => $cuentas, 'transacciones' => $transacciones]);
    }
    public function abonoDebito(Request $request)
    {
        $request->user()->authorizeRoles(['usuario']);
        $cuentas = Cuenta::where('usuario_id', Auth::user()->id)->where('tipocuenta_id',2)->paginate(1);
        $transacciones = Transaccion::where('usuario_id', Auth::user()->id)->where('cuenta_id',2)->paginate(10);
        return view('cuentas.abono_debito', ['cuentas' => $cuentas, 'transacciones' => $transacciones]);
    }

    public function CantidadRetiroCredito(Request $request){

        $saldo = $request->get('saldo');
        $cantidad = $request->get('cantidad');
        $retencion = Retencion::select('cobro_disposicion')->get();
        foreach ($retencion as $r) {
            $comision = $cantidad * $r->cobro_disposicion;
        }
        $operacion = $saldo-$cantidad-$comision;

        if(($saldo<$cantidad || $saldo<($cantidad+$comision))){
            return "false";
        }else{
            $cuentas = Cuenta::select('id')->where('usuario_id', Auth::user()->id)->where('tipocuenta_id',1)->get();
            foreach ($cuentas as $cuenta) {
               $data = $cuenta->id;
            }
        //acutaliza el saldo de la cuenta
        $actualizaSaldoCredito = Cuenta::where('usuario_id', Auth::user()->id)
        ->where('tipocuenta_id',1)
        ->update(['total_credito' => $operacion]); 
        // crea una transaccion al momento de actualizar el nuevo saldo
        $inputs = [
                    'concepto' => 'Retiro de Efectivo',
                    'importe' => $cantidad,
                    'status' => '1',
                    'usuario_id' => Auth::user()->id,
                    'cuenta_id' => $data
                ];
        $creaTransaccion = Transaccion::create($inputs);
        return "true";
        } 
        //return redirect()->action('${App\Http\Controllers\CuentasController@retiroCredito}');
        
    }
    // realiza retiro de TDD
    public function CantidadRetiroDebito(Request $request){

        $saldo = $request->get('saldo');
        $cantidad = $request->get('cantidad');
        $operacion = $saldo-$cantidad;
        if($saldo<$cantidad){
            return "false";
        }else{
            $cuentas = Cuenta::select('id')->where('usuario_id', Auth::user()->id)->where('tipocuenta_id',2)->get();
            foreach ($cuentas as $cuenta) {
               $data = $cuenta->id;
            }
        //acutaliza el saldo de la cuenta
        $actualizaSaldoDebito = Cuenta::where('usuario_id', Auth::user()->id)
        ->where('tipocuenta_id',2)
        ->update(['total_debito' => $operacion]); 
        // crea una transaccion al momento de actualizar el nuevo saldo
        $inputs = [
                    'concepto' => 'Retiro de Efectivo',
                    'importe' => $cantidad,
                    'status' => '1',
                    'usuario_id' => Auth::user()->id,
                    'cuenta_id' => $data
                ];
        $creaTransaccion = Transaccion::create($inputs);
        return "true";
        } 
    }

    //Realiza deposito a TDD
    public function CantidadAbonoDebito(Request $request){

        $saldo = $request->get('saldo');
        $cantidad = $request->get('cantidad');
        $operacion = $saldo+$cantidad;
        
            $cuentas = Cuenta::select('id')->where('usuario_id', Auth::user()->id)->where('tipocuenta_id',2)->get();
            foreach ($cuentas as $cuenta) {
               $data = $cuenta->id;
            }
        //abono al saldo de la cuenta
        $abonoSaldoDebito = Cuenta::where('usuario_id', Auth::user()->id)
        ->where('tipocuenta_id',2)
        ->update(['total_debito' => $operacion]); 
        // realiza una transaccion al momento de abonar al nuevo saldo
        $inputs = [
                    'concepto' => 'Deposito de efectivo a Cuenta de Debito',
                    'importe' => $cantidad,
                    'status' => '1',
                    'usuario_id' => Auth::user()->id,
                    'cuenta_id' => $data
                ];
        $realizaTransaccionAbono = Transaccion::create($inputs);
        return "true";
        }
        

}
