{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    
@stop

@section('content')
    @if (Auth::user()->rol == 'admin')
    <h1>Panel de Administración</h1>
    <h3>Bienvenido Administrador: {{ Auth::user()->nombre }} {{ Auth::user()->apellido_paterno }} {{ Auth::user()->apellido_materno}}</h3>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Elige la operación a realizar</div>
        <div class="panel-body">
            <div class="row row-no-gutters" style="text-align:center">
                    <div class="col-xs-6 col-md-4"><a href="{{ route('usuarios.index')}}"><img src="/img/user.png" width="100px" height="75px"></a><br><strong>Cuentas - Usuario</strong></div>
                    {{-- <div class="col-xs-6 col-md-4"><a href="{{ route('admin.usuarios.retiro_credito')}}"><img src="/img/retiro-efectivo.png" width="100px" height="75px"></a><br><strong>Retiro - Credito</strong></div>
                    <div class="col-xs-6 col-md-4"><a href="{{ route('admin.usuarios.retiro_debito')}}"><img src="/img/retiro-efectivo.png" width="100px" height="75px"></a><br><strong>Retiro - Debito</strong></div> --}}
            </div>  
        </div>    
    </div>
    
    @else
    <h3>Bienvenido Cliente: {{ Auth::user()->nombre }} {{ Auth::user()->apellido_paterno }} {{ Auth::user()->apellido_materno}}</h3>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Cuentas registradas del usuario</div>
        <div class="panel-body">
            <div class="row row-no-gutters" style="text-align:center">
                    <div class="col-xs-6 col-md-3"><a href="{{ route('cuentas.index')}}"><img src="/img/cuentas.png" width="100px" height="75px"></a><br><strong>Cuentas - Usuario</strong></div>
                    <div class="col-xs-6 col-md-3"><a href="{{ route('cuentas.retiro_credito')}}"><img src="/img/retiro-efectivo.png" width="100px" height="75px"></a><br><strong>Retiro - Credito</strong></div>
                    <div class="col-xs-6 col-md-3"><a href="{{ route('cuentas.retiro_debito')}}"><img src="/img/retiro-efectivo.png" width="100px" height="75px"></a><br><strong>Retiro - Debito</strong></div>
                    <div class="col-xs-6 col-md-3"><a href="{{ route('cuentas.abono_debito')}}"><img src="/img/abono.png" width="100px" height="75px"></a><br><strong>Abono - Debito</strong></div>
            </div>  
        </div>    
    </div>
    
    @endif
       
    
    
   
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

<script src="{{ asset('/js/cuentas.js') }}" type="text/javascript"></script>
