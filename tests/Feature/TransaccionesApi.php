<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransaccionesApi extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login_api_transacciones()
    {
        $this->get('/v1/admin/api/transacciones')
        ->assertStatus(200)
        ->assertSee('Obtiene las transacciones');
    }
}
