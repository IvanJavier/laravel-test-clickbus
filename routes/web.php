<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Rutas RESTFUL
Route::resource('usuarios','Administrador\UsuarioController');
Route::resource('cuentas','CuentasController');
Route::get('retiro_credito',['as'=>'cuentas.retiro_credito', 'uses' => 'CuentasController@retiroCredito']);
Route::get('retiro_debito',['as'=>'cuentas.retiro_debito', 'uses' => 'CuentasController@retiroDebito']);
Route::get('abono_debito',['as'=>'cuentas.abono_debito', 'uses' => 'CuentasController@abonoDebito']);
Route::get('cantidad/retiro', 'CuentasController@CantidadRetiroCredito');
Route::get('cantidad/retiroDebito', 'CuentasController@CantidadRetiroDebito');
Route::get('cantidad/abonoDebito', 'CuentasController@CantidadAbonoDebito');

Route::resource('transacciones','TransaccionesController');

// API - consulta todas las transacciones
Route::get('/v1/admin/api/transacciones', 'Administrador\Api\TransaccionesApiController@getApiTransacciones');
// API - consulta los usuarios
Route::get('/v1/admin/api/usuarios', 'Administrador\Api\UsuarioApiController@index');


