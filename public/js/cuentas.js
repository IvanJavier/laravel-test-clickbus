function retiroDebito(){
    saldo=parseInt($('#saldoinicial').val());
    cantidad=parseInt($('#cantidadretiro').val());
   if($('#cantidadretiro').val() == "" || cantidad<=0){
    swal({
      showConfirmButton: false,
      title: "Oppppsss!",
      text: "Error en el campo retiro",
      icon: "error",
    });
   }else{
    $.ajax({
      type: "GET",
      url: "cantidad/retiroDebito",
      data: {saldo,cantidad},
      beforeSend: function(xhr) {
          swal("¡Procesando información... !", {
              buttons: false,
            });
      },
      success: function(result) {
         if (result == "true"){
          swal({
              showConfirmButton: false,
              title: "Genial :)",
              text: "¡Operación realizada exitosamente!",
              icon: "success",
            });
            
         }else if(result == "false"){
          swal({
              showConfirmButton: false,
              title: "Rayos :(",
              text: "¡La cantidad solicitada es mayor al saldo en tu cuenta!",
              icon: "error",
            });
         }else{

          console.log(result);
         }
         $('#cantidadretiro').val('');
         location.reload();
    },
    error: function(xhr){
        swal({
            showConfirmButton: false,
            title: "Oppppsss!",
            text: "Hubo algunos problemas, intente de nuevo",
            icon: "error",
            timer: 4000,
          });
    }
  }); 
   }
} 



function retiroCredito(){
    //alert("entre retiro");
    saldo=parseInt($('#saldoinicial').val());
    cantidad=parseInt($('#cantidadretiro').val());
    if($('#cantidadretiro').val()=="" || cantidad<=0){
      swal({
        showConfirmButton: false,
        title: "Oppppsss!",
        text: "Error en el campo retiro",
        icon: "error",
      });
    }else{
      $.ajax({
        type: "GET",
        url: "cantidad/retiro",
        data: {saldo,cantidad},
        beforeSend: function(xhr) {
            swal("¡Procesando información... !", {
                buttons: false,
              });
        },
        success: function(result) {
           if (result == "true"){
            swal({
                showConfirmButton: false,
                title: "Genial :)",
                text: "¡Operación realizada exitosamente!",
                icon: "success",
              });
              
           }else if(result == "false"){
            swal({
                showConfirmButton: false,
                title: "Rayos :(",
                text: "¡La cantidad solicitada es mayor al saldo en tu cuenta!",
                icon: "error",
              });
           }else{

            console.log(result);
           }
           $('#cantidadretiro').val('');
           location.reload();
      },
      error: function(xhr){
          swal({
              showConfirmButton: false,
              title: "Oppppsss!",
              text: "Hubo algunos problemas, intente de nuevo",
              icon: "error",
              timer: 4000,
            });
      }
    });
    }
}

function abonoDebito(){
  saldo=parseInt($('#saldoinicial').val());
  cantidad=parseInt($('#cantidadabono').val());
 if($('#cantidadabono').val() == "" || cantidad<=0){
  swal({
    showConfirmButton: false,
    title: "Oppppsss!",
    text: "Ingresa una cifra correcta",
    icon: "error",
  });
 }else{
  $.ajax({
    type: "GET",
    url: "cantidad/abonoDebito",
    data: {saldo,cantidad},
    beforeSend: function(xhr) {
        swal("¡Procesando información... !", {
            buttons: false,
          });
    },
    success: function(result) {
       if (result == "true"){
        swal({
            showConfirmButton: false,
            title: "Genial :)",
            text: "¡Se realizo el deposito a tu cuenta!",
            icon: "success",
          });
          
       }else{

        console.log(result);
       }
       $('#cantidadabono').val('');
       location.reload();
  },
  error: function(xhr){
      swal({
          showConfirmButton: false,
          title: "Oppppsss!",
          text: "Hubo algunos problemas, intente de nuevo",
          icon: "error",
          timer: 4000,
        });
  }
}); 
 }
} 