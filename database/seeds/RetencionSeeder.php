<?php

use Illuminate\Database\Seeder;

class RetencionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Inserta datos a la tabla retencion
        $retenciones = [
            [
                
                'cobro_disposicion' => 0.1,
                'descripcion' => 'Cobro por disposición de efectivo',
                'concepto' => 'Aplica el 10% con TDC',
            ]
        ];

        DB::table('retenciones')->insert($retenciones);
    }
}
