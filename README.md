# Test Clickbus

_Prueba de conocimientos y habilidades._

## Requerimientos
- ** PHP => 7 **
- ** Laravel **
- ** MySQL **


### Instalación

Abrir terminal donde se quiera tener el proyecto
- **Clonar Repositorio **
```
git clone https://IvanJavier@bitbucket.org/IvanJavier/laravel-test-clickbus.git
```

Crear archivo .env 
- ** En la raiz del proyecto ejecutar**
```
cp .env.example .env
```

Configurar archivo .env 
- ** Abrir archivo y configurar**
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT="3307" ---> Puerto
DB_DATABASE="test_clickbus" ---> Nombre de la BD
DB_USERNAME="root" ----> usuario
DB_PASSWORD="" --> contraseña

```
Instalar dependencias
- ** En la raiz del proyecto ejecutar (Se debe tener instalado composer)**
```
composer install
```

Crear Base de Datos
- ** Nombre de la base de datos **
```
test_clickbus
```

Ejecutar migraciones
- ** Dentro del proyecto ejecutar el comando**
```
php artisan migrate
```

Ejecutar seeder
- ** Dentro del proyecto ejecutar el comando**
```
php artisan db:seed
```

Levantar aplicación
- ** El servidor que se ocupara para ejecutar la aplicación es el de Artisan - dentro del proyecto ejecutar**
```
php artisan serve
```

### Acceso al sistema

Rol Adminstrador
     email: administrador@administrador.com
     contraseña: 123456

Rol Usuario
     email: usuario@usuario.com
     contraseña: 123456


Peticiones API
     v1/admin/api/transacciones
     v1/admin/api/usuarios

_Ejecutar pruebas unitarias_
    - ** En la raiz del proyecto ejecutar**
```
vendor/bin/phpunit
```
## Rererencias

- **[Laravel](https://laravel.com/)**
- **[MySQL](https://www.mysql.com/)**
- **[Laracast](https://laracasts.com/)**
- **[Admin LTE](https://adminlte.io/)**
- **[Bootstrap](https://getbootstrap.com/docs/3.3/)**
- **[Laravel - PHP Unit](https://laravel.com/docs/5.8/testing)**

## Author

 Alvaro Iván Carrillo Javier

## License
Aplicación creada con la version Laravel 5.8

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
