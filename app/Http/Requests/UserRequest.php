<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:255',
            'apellido_paterno' => 'nullable|max:255',
            'apellido_materno' => 'nullable|max:255',
            'genero' => 'nullable|email|max:255',
            'telefono' => 'nullable|max:10',
            'email' => 'required|max:255',
            'password' => 'required|max:255',
            'rol' => 'nullable|max:2',
            'estatus' => 'required|numeric',
        ];
    }

    /**
     * @return array --> atributos del user
     */
    public function attributes(){
        return [
            'nombre' => 'Nombre',
            'apellido_paterno' => 'Apellido paterno',
            'apellido_paterno' => 'Apellido materno',
            'genero' => 'Genero',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'password' => 'Contraseña',
            'rol' => 'Rol',
            'estatus' => 'Estatus'
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        if($validator->fails()) {
            throw new HttpResponseException(redirect()->back()->withErrors($validator));
        }
    }
}
