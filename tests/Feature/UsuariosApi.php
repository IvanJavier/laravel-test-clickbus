<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsuariosApi extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login_api_usuarios()
    {
        $this->get('/v1/admin/api/usuarios')
        ->assertStatus(200)
        ->assertSee('Obtiene los usuarios');
    }
}
