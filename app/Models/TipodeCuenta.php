<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TipodeCuenta extends Model
{
    use Notifiable;

    
    protected $table = 'tipo_de_cuenta';

    /**
     * 
     *
     * @var array
     */
    protected $fillable = [
        'descripcion','tipo_tarjeta','tipo_cambio'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
