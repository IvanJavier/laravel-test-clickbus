@if (Auth::user()->rol=="admin")
    {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Usuarios del Sistema</h1>
@stop

@section('content')
    
    <div class="box box-solid box-primary">
        <h4>Bienvenido: {{ Auth::user()->nombre }} {{ Auth::user()->apellido_paterno }}  {{ Auth::user()->apellido_materno }} </h4>
    </div>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        
        <thead>
            <tr>
                <th>Tipo de Usuario</th>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Telefono</th>
                <th>Email</th>
                <th>Estatus</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($usuarios as $usuario)
                <tr>
                <td>{{ $usuario->rol }}</td>
                <td>{{ $usuario->nombre }}</td>
                <td>{{ $usuario->apellido_paterno }}</td>
                <td>{{ $usuario->apellido_materno }}</td>
                <td>{{ $usuario->telefono }}</td>
                <td>{{ $usuario->email }}</td>
                @if ($usuario->estatus =1)
                <td><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>   
                @else
                <td><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td>     
                @endif
                <td><a class="btn btn-primary" href="{{-- {{ route('admin.usuarios.edit')}} --}}">Editar</a></td> 
                
                <td><a class="btn btn-primary" href="{{-- {{ route('admin.usuarios.destoy')}} --}}">Eliminar</a></td> 
                
                </tr>
            @endforeach
                
        </tbody>
    </table>
    <a href="{{route('home')}}">< Regresar</a>
    
   
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
@else
    @include('layouts.404')
@endif