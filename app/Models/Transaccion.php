<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Transaccion extends Model
{
     use Notifiable;
     // tabla
     protected $table = 'transacciones';
 
     /**
      *
      * @var array
      */
     protected $fillable = [
         'importe','concepto','status','usuario_id', 'cuenta_id'
     ];
 
     /**
      * The attributes that should be mutated to dates.
      *
      * @var array
      *
      */
     protected $dates = ['created_at', 'updated_at'];
 
     // foreing key -> cuenta_id
     public function cuenta()
     {
         return $this->belongsTo('App\Models\Cuenta', 'cuenta_id');
     }
     
     // foreign key usuario_id
     public function usuario()
     {
         return $this->belongsTo('App\User', 'usuario_id');
     }
}
