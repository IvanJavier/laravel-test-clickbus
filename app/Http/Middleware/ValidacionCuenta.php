<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Cuenta;

class ValidacionCuenta
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Obtiene cuenta_id
        $cuenta_id = Cuenta::select('cuentas.usuario_id')
                    ->where('id',$request->id)
                    ->get();

        foreach ($cuenta_id as $id) {
            if (!($id->usuario_id == Auth::user()->id)) {
                return redirect()->route('denied');
            }
        return $next($request);
    }
}
