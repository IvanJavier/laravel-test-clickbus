<?php

use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = [
            [
                'id' => 1,
                'rol_id' => 1, //Rol administrador
                'user_id' => 1, // tipo administrador
            ],
            [
                'id' => 2,
                'rol_id' => 2, //Rol Usuario
                'user_id' => 2, // tipo ---> usuario
            ]
        ];

        //inserta la informacion
        DB::table('rol_user')->insert($role_user);
    }
}
