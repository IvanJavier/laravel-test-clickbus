<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginCajero extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login_acceso()
    {
        $this->get('/login')
        ->assertStatus(200)
        ->assertSee('Ruta del login');
    }
}
