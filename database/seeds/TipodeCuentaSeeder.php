<?php

use Illuminate\Database\Seeder;

class TipodeCuentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // tipo de cuenta/tarjeta
        $tipo_de_cuenta = [
            [
                'descripcion' => 'Tarjeta de Credito',
                'tipo_tarjeta' => 'credito',
                'tipo_cambio' => 'mxn',
                
            ],
            [
                'descripcion' => 'Tarjeta de Debito',
                'tipo_tarjeta' => 'debito',
                'tipo_cambio' => 'mxn',
            ]
        ];

        DB::table('tipo_de_cuentas')->insert($tipo_de_cuenta);
    }
}
