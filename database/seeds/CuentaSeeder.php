<?php

use Illuminate\Database\Seeder;

class CuentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Agrega información referente a las tarjetas/cuentas de un usuario
        $cuentas_usuario = [
            [
                'num_cuenta' => Str::random(16),
                'clabe' => Str::random(18),
                'banco' => 'Bancomer',
                'rfc' => 'JAOL150293LM3',
                'total_credito' => 50000,
                'total_debito' => 0,
                'retenciones_id' => 1,
                'tipocuenta_id' => 1,// CREDITO
                'usuario_id' => 2,// rol -> usuario
            ],
            [
                'num_cuenta' => Str::random(16),
                'clabe' => Str::random(18),
                'banco' => 'Bancomer',
                'rfc' => 'JAOL150293LM3',
                'total_credito' => 0,
                'total_debito' => 30000,
                'retenciones_id' => null,
                'tipocuenta_id' => 2,// DEBITO
                'usuario_id' => 2, //rol -> usuario
            ]
        ];

        //inserta la informacion
        DB::table('cuentas')->insert($cuentas_usuario);
    }
}
