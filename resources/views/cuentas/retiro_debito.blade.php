{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Retiro de Efectivo - Debito</h1>
@stop

@section('content')
    
    <table id="tableDebito" class="table table-striped table-bordered" style="width:100%">
        
        <thead>
            <tr>
                <th>Numero de Cuenta</th>
                <th>Clabe</th>
                <th>Institución</th>
                <th>RFC</th>
                <th>Saldo</th>
                <th>Tipo Cuenta</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($cuentas as $cuenta)
            <tr>
            <td>{{ $cuenta->num_cuenta}}</td>
            <td>{{ $cuenta->clabe}}</td>
            <td>{{ $cuenta->banco}}</td>
            <td>{{ $cuenta->rfc}}</td>
            <td>{{ $cuenta->total_debito}}</td> 
            <td>Debito</td>
            </tr>
        @endforeach            
                
        </tbody>
    </table>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Realizar Retiro de tu TDD</div>
        <div class="form-group">
            {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
            <label for="saldoinicial">Saldo Disponible</label>
            <input class="form-control" disabled="true" type="text" id="saldoinicial" value="{{ $cuenta->total_debito }}">
            <label for="cantidadretiro">Cantidad a retirar</label>
            <input class="form-control" type="text" id="cantidadretiro"><br>
            <input type="button" class="btn btn-primary" href="#" id="retiroedfectivo" onclick="retiroDebito()" value="Retirar Efectivo">
        </div> 
    </div>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Historial de Transacciones</div>
        <table id="transacciones" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Concepto</th>
                    <th>Importe</th>
                    <th>Status</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                @if (count($transacciones)<=0)
                    <tr>
                        <td>Sin movimientos<td>
                    </tr>
                @else
                    @foreach ($transacciones as $transaccion)
                    <tr>
                    <td>{{ $transaccion->concepto}}</td>
                    <td>{{ $transaccion->importe}}</td>
                    @if ($transaccion->status=1)
                    <td><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>   
                    @else
                    <td><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td>     
                    @endif
                    <td>{{ $transaccion->created_at}}</td> 
                    </tr>
                    @endforeach  
                @endif         
            </tbody>
        </table> 
        {{ $transacciones->links() }}
    </div>
    <a href="{{route('home')}}">< Regresar</a>
    <script src="{{ asset('/js/cuentas.js') }}" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>  
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop