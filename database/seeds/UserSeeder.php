<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $usuarios = [
            [
                'nombre' => 'Iván',
                'apellido_paterno' => 'Carrillo',
                'apellido_materno' => 'Javier',
                'genero' => 'M',
                'telefono' => Str::random(10),
                'email' => 'administrador@administrador.com',
                'password' => bcrypt('123456'),
                'rol' => 'admin',
                'estatus' => 1,
                
            ],
            [
                'nombre' => 'Juan',
                'apellido_paterno' => 'Perez',
                'apellido_materno' => 'Lopez',
                'genero' => 'M',
                'telefono' => Str::random(10),
                'email' => 'usuario@usuario.com',
                'password' => bcrypt('123456'),
                'rol' => 'usuario',
                'estatus' => 1,
            ]
        ];

        DB::table('users')->insert($usuarios);
    }
}
